<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'     => 'movies',
    'middleware' => 'jwt.auth',
], function(){

     Route::post('shift-assign', 'MovieController@shift_assign');


});