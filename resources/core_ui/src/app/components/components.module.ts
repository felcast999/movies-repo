import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailHeaderComponent } from './detail-header/detail-header.component';

@NgModule({
  declarations: [
    DetailHeaderComponent
  ],
  exports: [
    DetailHeaderComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
