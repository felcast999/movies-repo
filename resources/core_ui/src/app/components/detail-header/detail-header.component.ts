import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'detail-header',
  templateUrl: './detail-header.component.html',
  styleUrls: ['./detail-header.component.scss']
})
export class DetailHeaderComponent
{
  @Input()
  public title:string = "";
  
  @Input()
  public subtitle:string = "";
  
  @Input()
  public icon:string = "";

  constructor(
    private location:Location
  ) { }

  public back():void
  {
    this.location.back();
  }

}
