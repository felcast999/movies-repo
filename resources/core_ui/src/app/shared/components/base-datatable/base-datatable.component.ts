import { Component, OnInit, OnDestroy, ViewChild, EventEmitter, Output  } from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import { AppDataService } from '../../../services/app-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'base-datatable',
  templateUrl: './base-datatable.component.html',
  styleUrls: ['./base-datatable.component.scss']
})
export class BaseDatatableComponent implements OnInit, OnDestroy
{

  @ViewChild(DataTableDirective)
  public dtElement: DataTableDirective;

  dtOptions: any = {}; 
  public dtTrigger: Subject<any> = new Subject(); 
      
  @Output() 
  public delete: EventEmitter<any> = new EventEmitter()

  protected dataSubscription: Subscription; 
  
  constructor(
    public _appDataService: AppDataService,
    public router:Router
   )
  {
  }

  ngOnInit():void
  {
    this.setDtOptions();
  }

  ngOnDestroy():void
  {   
    this.dtTrigger.unsubscribe();
    this.dataSubscription.unsubscribe();
  }

  public init():void
  {
    // las opciones de la tabla vienen de una peticion http y se debe 
    // esperar a que se resuelva la promesa.
    setTimeout(() => this.dtTrigger.next(), 1000);
  }
  
  public reload(): void
  { 
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload(null, false);
    });
  }

  public async setDtOptions():Promise<void>
  {
    this.dtOptions = await this._appDataService.getDatatableSettings(); 
  }
 
}
