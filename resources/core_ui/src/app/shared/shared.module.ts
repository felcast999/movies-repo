import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

//import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { DataTablesModule } from 'angular-datatables';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgSelectModule } from '@ng-select/ng-select';
//import { OwlDateTimeModule, OwlNativeDateTimeModule, OwlDateTimeIntl} from 'ng-pick-datetime';

import { ComponentsModule } from '../components/components.module';
import { BaseDatatableComponent } from './components/base-datatable/base-datatable.component';

@NgModule({
  imports: [
    CommonModule,
   // Ng4LoadingSpinnerModule.forRoot(),
    DataTablesModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    NgSelectModule,
   // OwlDateTimeModule, 
    //OwlNativeDateTimeModule,
    ComponentsModule
  ],
  exports: [
    CommonModule,
   // Ng4LoadingSpinnerModule,
    DataTablesModule,
    ModalModule,
    FormsModule,
    NgSelectModule,
    //OwlDateTimeModule, 
   // OwlNativeDateTimeModule,
    ComponentsModule
  ],
  declarations: [BaseDatatableComponent]
})
export class SharedModule { }
