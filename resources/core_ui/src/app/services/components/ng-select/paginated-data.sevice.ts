import { Injectable } from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {parseToUrlParams} from '../../../shared/helpers';

@Injectable()
export class PaginatedDataService
{
	private baseUrl:string;
	private apiUrl:string;
	public subject:BehaviorSubject<any>;
	public data$:Observable<any>;
	public data:any[] = [];

	constructor(
		private _http: HttpClient
		)
	{
		this.baseUrl = environment.APIEndpoint;
		this.apiUrl = null;
		this.subject = new BehaviorSubject<any>({
			data: [],
			to: 0,
			total: 0
		});
		this.data$ = this.subject.asObservable();
	}

	public setApiUrl(apiUrl:string, params?:{}):void
	{
		this.apiUrl = this.baseUrl + apiUrl + (params ? parseToUrlParams(params) : '');
	}

	public hasApiUrl():boolean
	{
		return this.apiUrl != null;
	}

	public fetchMore():void
	{
		if (this.thereMoreData())
		{
			this._http.get(this.apiUrl)
				.toPromise()
				.then((response:any) => {

					this.data = [...this.data, ...response.data];

					let data = {
						data: this.data,
						to: response.to,
						total: response.total
					};

					this.apiUrl = response.next_page_url;

					this.updateData(data);
				});
		}
	}

	private updateData(data:any):void
	{
		this.subject.next(data);
	}

	private thereMoreData():boolean
	{
		const values = this.subject.getValue();

		return values.total > 0 ? values.to < values.total : true;
	}
}
