import { Injectable } from '@angular/core';
import {
	Resolve,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';

import { ShiftService } from '../../shift.service';
import { Shift } from '../../../models/shift';
import { map,catchError } from 'rxjs/operators';

@Injectable()
export class ShowResolverService implements Resolve<any>
{
  constructor(
  	private _shiftService:ShiftService
  ) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Shift|any>
  {
      const id = Number(route.paramMap.get('id'));

  		return this._shiftService
  					   .show(id);
  }





}

