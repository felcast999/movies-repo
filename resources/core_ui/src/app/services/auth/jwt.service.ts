import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn:'root'
})

export class JwtService {

	public url:string;	
	
	constructor(
		private http: HttpClient
	)
	{
		this.url = environment.host;		
	}

	private setAuthenticationHeaders(): object
	{
	    const token = localStorage.getItem('token'),
	          httpHeaders = {
	            headers: new HttpHeaders({
	              'Authorization': `Bearer  ${token}`
	            })
	          };

	    return httpHeaders;
	}

	login(data:any)
	{
		return this.http.post(`${this.url}/login`, data);
	}

	logout()
	{
		return this.http.post(`${this.url}/logout`,{}, this.setAuthenticationHeaders());
	}

	 register(data:any)
	 {
	 	return this.http.post(`${this.url}/register`, data);
	}

	sendResetPasswordLink(data:any)
	{
		return this.http.post(`${this.url}/sendResetPasswordLink`, data);
	}

	changePassword(data:any)
	{
		return this.http.post(`${this.url}/changePassword`, data);
	}

	checkIfTokenIsValid()
	{	
		return this.http.get(`${this.url}/checkIfTokenIsValid`, this.setAuthenticationHeaders());

	}

	getAuthUser()
	{
		return this.http.get(`${this.url}/me`, this.setAuthenticationHeaders());	
	}

}
