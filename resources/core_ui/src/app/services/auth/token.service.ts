import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';

@Injectable()

export class TokenService {

  private iss: object;

  constructor()
  {
  	this.iss = {
  		login: `${environment.host}/login`,
  		register: `${environment.host}/register`,
  	};
  }

  handle(token)
  {
  	this.setToken(token);
  }

  setToken(token) :void
  {
  	localStorage.setItem('token',token);
  }

  getToken() :string
  {
  	 return localStorage.getItem('token');
  }

  removeToken() :void
  {
  	 localStorage.removeItem('token');
  }

  isValid()
  {
  	const token = this.getToken();

  	if (token)
  	{
  		const payload = this.payload(token);
  		
  		if (payload)
  			return Object.values(this.iss).indexOf(payload.iss) !== -1;
  		
  		return false;
  	}

    return false;

  }

  payload(token)
  {
  	const payload = token.split('.')[1]; 
  	return this.decode(payload);
  }

  decode(payload)
  {
  	return JSON.parse(atob(payload));
  }

  loggedIn()
  {
  	return this.isValid();
  }
}
