import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorResponseService {

  constructor(
  	private toastr: ToastrService,    
  	private _authService: AuthService,
    private _router: Router
  ) { }

  public handle(error, callback=null):any
  {
    console.log(error);

    if (callback)
    	callback();
      
      if (this._router.url !== "/login")
      {
        switch(true)
        {
          case (error.status === 500 || error.status === 0):
            
            if (!this.toastr.currentlyActive)
              this.toastr.error(error.error.message, "Error interno.");
            
              this._router.navigateByUrl('500');

              break;
          
          case error.status === 403:
              
              return this.toastr.error("Este usuario no tiene los permisos requeridos para realizar esta accion","No autorizado");      
              break;

          case error.status === 401:

            	return this._authService.closeSession('Sesion expirada','Inicie sesion nuevamente','error');
              break;
      
          default:

             const errors = error.error.errors;
             
             Object.values(errors).forEach((error:string) => {

                 this.toastr.error(error[0], 'Datos invalidos.');
             });

             break;
        }
    }

  }

}
