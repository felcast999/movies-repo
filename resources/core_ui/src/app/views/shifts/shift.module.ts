import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ShiftRoutingModule } from './shift-routing.module';

import { ShiftComponent } from './shift.component';
import { TableComponent } from './table/table.component';
import { CreationModalComponent } from './creation-modal/creation-modal.component';
import { DetailsViewComponent } from './details-view/details-view.component';

import { ShiftService } from '../../services/shift.service';
import {  AuthInterceptor } from '../../services/http-interceptors/auth-interceptor.service';


import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { DataTablesModule } from 'angular-datatables';
import { HttpClient, HttpHeaders, HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {BrowserModule} from '@angular/platform-browser';


@NgModule({
  declarations: [
    ShiftComponent,
    CreationModalComponent,
    TableComponent,
    DetailsViewComponent,
    ],
  imports: [
    ShiftRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
  ],
  providers: [
    ShiftService,
    { 
   provide: HTTP_INTERCEPTORS,
   useClass: AuthInterceptor, 
   multi: true 
    }
  ]
})
export class ShiftModule { }
