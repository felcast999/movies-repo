import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShiftComponent } from './shift.component';
import { DetailsViewComponent } from './details-view/details-view.component';

// resolvers
import { ShowResolverService } from '../../services/solvers/shift/show-resolver.service';


const routes: Routes = [{
  path: '',
  data: {
    title: 'Turnos'
  },
  children: [
    
    {
      path: '',
      component: ShiftComponent,
      data: {
        title: "Turnos"
      }
    },
    {
      path: ':id',
      component: DetailsViewComponent,
      data: {
        title: "Detalles"
      },
      resolve: {
        model: ShowResolverService
      }
    }
  ] 
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ShowResolverService
  ]
})
export class ShiftRoutingModule { }
