import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { formatDate } from '../../../shared/helpers';

@Component({
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.scss']
})
export class DetailsViewComponent implements OnInit {

	public shift;

  constructor(
  	private route:ActivatedRoute
  ) { }

  ngOnInit()
  {
  	this.route.data.subscribe((data:any) =>{ 
      this.shift = data.model.shift
    });
  }

  public formatDate(date:string|Date, format?:string):string
  {
  		return formatDate(date,format);
  }
}  
