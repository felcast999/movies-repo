import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {JwtService} from '../../services/auth/jwt.service';
import {SnotifyService} from 'ng-snotify';
import { Observable } from 'rxjs';

@Component({
  selector: 'body',
  templateUrl: './response-password-reset.component.html',
  styleUrls: ['./response-password-reset.component.css']
})
export class ResponsePasswordResetComponent implements OnInit {

	public form: any;
	public btnText = 'Enviar';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  	private _jwtService : JwtService, 
  	private notify: SnotifyService
  )
  {
  	this.form = {
  		email: null,
  		password: null,
  		password_confirmation: null,
  		resetToken: null
  	}; 

  }

  ngOnInit(): void
  {
  	this.route.queryParams.subscribe(params => {
      this.form.email = params['email'];
      this.form.resetToken = params['token'];
  	});
  }

  onSubmit()
  {
    if (this.btnText === 'Ir al login')
      return this.router.navigateByUrl('/login');

    const action = Observable.create(observer => {
          
          observer.next({
            title: 'Espere...',
            body: 'Enviando solicitud...',
          });

          this._jwtService.changePassword(this.form).subscribe(
            response => this.handleResponse(response, observer),
            error => this.handleError(error, observer) 
          );

        });

    this.notify.async('',action,{ 
        closeOnClick: true,
        timeout: 6000
      });
  }

  handleResponse(data, observer)
  {
    observer.next({
      title: 'Exito!',
      body: data.message
    });

    observer.complete();

    // redireccionar al login en 6 segundos despues de ver el mensaje 
    setTimeout(() => {
      this.router.navigateByUrl('/login')
    }, 6000);

    this.btnText = 'Ir al login'; 
  }

  handleError(error, observer)
  {
  	  console.log(error);
      
      const errors = error.error.errors;
      
      if (error.status === 500 || error.status === 0)
      {
        observer.error({
            title: 'Error',
            body: 'Verifique su conexion',
        });
      }
      else
      {
        observer.error({
            title: 'Error',
            body: 'Solicitud fallida',
        });

        Object.values(errors).forEach(error => this.notify.error(error[0],'Error',{showProgressBar:false, timeout:7000}));
      }

      this.btnText = 'Enviar'; 

  }

}
