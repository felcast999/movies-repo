import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MovieRoutingModule } from './movie-routing.module';

import { MovieComponent } from './movie.component';
import { TableComponent } from './table/table.component';
import { CreationModalComponent } from './creation-modal/creation-modal.component';
import { DetailsViewComponent } from './details-view/details-view.component';
import { AssignModalComponent } from './assign-modal/assign-modal.component';

import { MovieService } from '../../services/movie.service';
import { ShiftService } from '../../services/shift.service';

import {  AuthInterceptor } from '../../services/http-interceptors/auth-interceptor.service';


import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { DataTablesModule } from 'angular-datatables';
import { HttpClient, HttpHeaders, HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {BrowserModule} from '@angular/platform-browser';


@NgModule({
  declarations: [
    MovieComponent,
    CreationModalComponent,
    TableComponent,
    DetailsViewComponent,
    AssignModalComponent
    ],
  imports: [
    MovieRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    NgSelectModule
  ],
  providers: [
    MovieService,
    ShiftService,
    { 
   provide: HTTP_INTERCEPTORS,
   useClass: AuthInterceptor, 
   multi: true 
    }
  ]
})
export class MovieModule { }
