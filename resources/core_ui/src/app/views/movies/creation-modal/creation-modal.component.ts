import { Component, Input, Output, ViewChild, EventEmitter, OnInit,ElementRef } from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';

// services
import swal from 'sweetalert2';
import { MovieService as CRUDService } from '../../../services/movie.service';

import { showPreconfirmMessage } from '../../../shared/helpers';
import { Observable } from 'rxjs';

import { MovieForm as FormModel } from '../../../form-models/movie-form';
import { DateTimeAdapter } from 'ng-pick-datetime';
import {environment} from '../../../../environments/environment';


@Component({
  selector: 'creation-modal',
  templateUrl: './creation-modal.component.html',
  styleUrls: ['./creation-modal.component.css'],
  providers: [
  ]
})
export class CreationModalComponent implements OnInit {

  @ViewChild('modalCR')
  public modal: ModalDirective;
  
  @ViewChild('file_input', { static: true }) input: ElementRef;


  @ViewChild('formCR')
  public form:NgForm;
  
  @Output()
  public reloadTable: EventEmitter<any> = new EventEmitter();

  public loadingBatches:boolean;
  
  public sendingForm:boolean;


  public form_model:any;

  public model_form_control:any;
  
  public imgFile: string;

  public baseUrl:string;

 


  constructor(
    private _crudService: CRUDService,
    private fb: FormBuilder,
    private dateTimeAdapter: DateTimeAdapter<any>

   )
  {
    this.baseUrl = environment.APIStorage;

    // cambiar configuracion regional a español en datetimepicker
    this.dateTimeAdapter.setLocale('es-ES'); 


    this.form_model= new FormModel(this.fb);



    this.model_form_control= this.form_model.model_form_control;

    this.sendingForm = false;

    this.loadingBatches = false;
  }

  ngOnInit():void
  {
  }


public onImageChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        this.imgFile = reader.result as string;

   
          this.form_model.form.controls['cover'].setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.toString().split(',')[1] 
        })

      }

    }

  }



 
  public async showPreconfirmMessage():Promise<void>
  {

  let message:string='';

  switch(this.model_form_control._method.value) { 
   case 'POST': { 

      message='Añadir Pelicula?';

      break; 
   } 
   case 'PUT': { 
      message='Modificar Pelicula?';

      break; 
   } 
  
   } 


  const response = await showPreconfirmMessage(
                            message,
                            "",
                            "info",
                            "Cancelar",
                            "Si"
                            );

    if (response.value)
      this.sendForm();
  }

  private sendForm():void
  {
  	if (this.sendingForm)
  		return;

    this.sendingForm = true;


   switch(this.form_model.model_form_control._method.value) { 
   case 'POST': { 

         this._crudService.store(this.form_model.form.value).subscribe(
      response => this.handleResponse(response),
      err =>{
        console.error('Observer got an error: ' + err)
            this.sendingForm = false

    },
      () => {
    this.sendingForm = false

      }
     );

      break; 
   } 
   case 'PUT': { 
         this._crudService.update(this.form_model.form.value).subscribe(
      response => this.handleResponse(response),
      err =>{
        console.error('Observer got an error: ' + err)
            this.sendingForm = false

    },
      () => {
    this.sendingForm = false

      }
     );
      break; 
   } 
  
   } 



   



  }


  public clean()
  {
    console.log(this.input)
    this.input.nativeElement.value=""
  }


  private handleResponse(response):void
  {
    const action = () => {
      this.reloadTable.emit(true);
      this.modal.hide();
    };

    swal.fire('Exito!',response.message,'success')
        .then( () =>  action())
        .catch( () => action());
  }

  public clearForm():void
  {
    this.form_model.form.reset();

    this.sendingForm = false;
   


  }

}
