import { Component, Input, Output, ViewChild, EventEmitter, OnInit } from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';

// services
import swal from 'sweetalert2';
import { MovieService as CRUDService } from '../../../services/movie.service';

import { showPreconfirmMessage } from '../../../shared/helpers';
import { Observable } from 'rxjs';

import { AssignForm as FormModel } from '../../../form-models/assign-form';


@Component({
  selector: 'assign-modal',
  templateUrl: './assign-modal.component.html',
  styleUrls: ['./assign-modal.component.css'],
  providers: [
  ]
})
export class AssignModalComponent implements OnInit {

  @ViewChild('modalAS')
  public modal: ModalDirective;
  
  @ViewChild('formAS')
  public form:NgForm;
  
  @Output()
  public reloadTable: EventEmitter<any> = new EventEmitter();

  public loadingBatches:boolean;
  
  public sendingForm:boolean;

  public shifts:any;

  public form_model:any;

  public model_form_control:any;


  constructor(
    private _crudService: CRUDService,
    private fb: FormBuilder
   )
  {


     this.form_model= new FormModel(this.fb);

     this.model_form_control= this.form_model.model_form_control;

    this.sendingForm = false;

    this.loadingBatches = false;
  }

  ngOnInit():void
  {
  }

 


 
  public async showPreconfirmMessage():Promise<void>
  {

  let message:string='';

  switch(this.model_form_control._method.value) { 
   case 'POST': { 

      message='Añadir Turno?';

      break; 
   } 
   
  
   } 


  const response = await showPreconfirmMessage(
                            message,
                            "",
                            "info",
                            "Cancelar",
                            "Si"
                            );

    if (response.value)
      this.sendForm();
  }

  private sendForm():void
  {
  	if (this.sendingForm)
  		return;

    this.sendingForm = true;

    console.log(this.form_model.model_form_control._method.value);

   switch(this.form_model.model_form_control._method.value) { 
   case 'POST': { 

         this._crudService.assign(this.form_model.form.value).subscribe(
      response => this.handleResponse(response),
      err =>{
        console.error('Observer got an error: ' + err)
            this.sendingForm = false

    },
      () => {
    this.sendingForm = false

      }
     );

      break; 
   } 
  
   } 



   



  }

  private handleResponse(response):void
  {
    const action = () => {
      this.reloadTable.emit(true);
      this.modal.hide();
    };

    swal.fire('Exito!',response.message,'success')
        .then( () =>  action())
        .catch( () => action());
  }

  public clearForm():void
  {
    this.form_model.form.reset();

    this.sendingForm = false;
   


  }

}
