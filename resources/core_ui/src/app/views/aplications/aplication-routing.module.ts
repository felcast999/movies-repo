import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AplicationComponent } from './aplication.component';
import { DetailsViewComponent } from './details-view/details-view.component';

// resolvers
import { ShowResolverService } from '../../services/solvers/device/show-resolver.service';


const routes: Routes = [{
  path: '',
  data: {
    title: 'Aplicación'
  },
  children: [
    
    {
      path: '',
      component: AplicationComponent,
      data: {
        title: "Aplicación"
      }
    },
    /*{
      path: ':id',
      component: DetailsViewComponent,
      data: {
        title: "Detalles de Dispositivo"
      },
      resolve: {
        model: ShowResolverService
      }
    }*/
  ] 
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ShowResolverService
  ]
})
export class AplicationRoutingModule { }
