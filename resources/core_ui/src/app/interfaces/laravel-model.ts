export interface LaravelModel
{
    readonly id:number;
    created_at:string;
    updated_at?:string;
}