import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';


export class ShiftForm {
 
 public form:FormGroup;



constructor(
		    private fb?: FormBuilder
	)
	{

     this.form = this.fb.group({
      _method:new FormControl('POST'),
      shift_id:new FormControl(null),
      shift_hour: new FormControl('',Validators.required),
      status: new FormControl('',Validators.required),

    });

	}



 get model_form_control() {
    return this.form.controls;
  }



  get is_invalid() {
    return this.form.invalid;
  }
  


}
