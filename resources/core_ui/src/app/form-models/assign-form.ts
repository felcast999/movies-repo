import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';


export class AssignForm {
 
 public form:FormGroup;



constructor(
		    private fb?: FormBuilder
	)
	{

     this.form = this.fb.group({
      _method:new FormControl('POST'),
      shift_ids: new FormControl('',Validators.required),
      movie_id: new FormControl('',Validators.required),

    });

	}



 get model_form_control() {
    return this.form.controls;
  }



  get is_invalid() {
    return this.form.invalid;
  }
  


}
