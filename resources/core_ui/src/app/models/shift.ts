export class Shift {
   
constructor(
		public id:number,
		public shift_hour:string,
		public status:string,
		public created_at?:string,
		public updated_at?:string,
	)
	{

	}


/* Methods */

	static createInstance(data:any)
	{
		const {id,shift_hour,status,created_at, updated_at} = data;
		return new Shift(id,shift_hour,status,created_at, updated_at);   
	}


}
