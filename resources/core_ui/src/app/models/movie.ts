export class Movie {
   
constructor(
		public id:number,
		public name:string,
		public publication_date:string,
		public status:string,
		public image:string,
		public created_at?:string,
		public updated_at?:string,
	)
	{

	}


/* Methods */

	static createInstance(data:any)
	{
		const {id,name,publication_date,status,image,created_at, updated_at} = data;
		return new Movie(id,name,publication_date,status,image,created_at, updated_at);   
	}


}
