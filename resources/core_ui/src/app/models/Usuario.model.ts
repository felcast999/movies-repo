import {Deserializable} from "./deserializable.model";

export class Usuario {
  numero_identificacion: number;
  nombre: string;
  nombre_comercial:string;
  telefono:string;
  correo:string;
  direccion:string;
  logo:string;


    deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}