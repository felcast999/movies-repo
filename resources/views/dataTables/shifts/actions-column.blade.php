<div class="row">
    
    <div class="col-12 p-0 mb-1">
        


        <button type="button" title="Modificar" data-id="{{$model->id}}" class="btn btn-md bg-success text-white update-model">
            <i class="fa fa-pencil update-model"></i>
        </button>

        <button type="button" title="Ver detalles" data-id="{{$model->id}}" class="btn btn-md bg-warning text-white details-model">
            <i class="fa fa-eye details-model"></i>
        </button>

      
        <button type="button" title="Eliminar" data-id="{{$model->id}}" class="btn btn-md bg-danger text-white delete-model">
                <i class="fa fa-trash delete-model"></i>
        </button>
        

    </div>
        
</div>
