<?php

declare(strict_types=1);

namespace App\Datatables;

use Yajra\DataTables\DataTables;
use Yajra\DataTables\EloquentDataTable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;

abstract class BaseDataTable extends Datatables
{
    protected $model;
    protected $query;
    protected $new_dataTable;

    public function __construct(Model $model)
    {
    	$this->model = $model;
    }

    public final function build():JsonResponse
    {
    	$this->makeQuery();

    	$this->newDataTable();

    	$this->makeColumns();

    	return $this->new_dataTable->make(true);
    }

    abstract protected function makeQuery():void;

    public function setQuery(Builder $query):void
	{
		$this->query = $query;
	}

	private final function newDataTable():void
	{
		$this->new_dataTable = self::of($this->query);
	}

	abstract protected function makeColumns():EloquentDataTable;
}
