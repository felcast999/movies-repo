<?php

namespace App\Datatables\Shift;

use App\Datatables\BaseDataTable;
use App\Models\Shift;
use Yajra\DataTables\EloquentDataTable;

class ShiftDataTable extends BaseDataTable
{
	public function __construct(Shift $model)
	{
		parent::__construct($model);
	}

	protected function makeQuery():void
	{	
		$query = $this->model
						->query()
                        ->select('shifts.*'); 

		$this->setQuery($query);
	}

	protected function makeColumns():EloquentDataTable
	{
		return $this->new_dataTable
					->addColumn('_shift_hour', function($model){

						return "<span class='badge bg-success p-2 middle-text-badge'>{$model->shift_hour}</span>";

                    })
                     ->addColumn('_status', function($model){

						return "<span class='badge bg-success p-2 middle-text-badge'>{$model->status}</span>";

                    })
					->addColumn('_actions', function($model){

						return view('dataTables.shifts.actions-column', compact('model'));

					})
					->rawColumns([
						'_shift_hour',
						'_status',
						'_actions'
					]);
	}

}