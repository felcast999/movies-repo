<?php

namespace App\Datatables\Movie;

use App\Datatables\BaseDataTable;
use App\Models\Movie;
use Yajra\DataTables\EloquentDataTable;

class MovieDataTable extends BaseDataTable
{
	public function __construct(Movie $model)
	{
		parent::__construct($model);
	}

	protected function makeQuery():void
	{	
		$query = $this->model
						->query()
                        ->select('movies.*'); 

		$this->setQuery($query);
	}

	protected function makeColumns():EloquentDataTable
	{
		return $this->new_dataTable
					->addColumn('_name', function($model){

						return "<span class='badge bg-success p-2 middle-text-badge'>{$model->name}</span>";

                    })
                    ->addColumn('_publication_date', function($model){

						return "<span class='badge bg-success p-2 middle-text-badge'>{$model->publication_date->toDateString()}</span>";

                    })
                     ->addColumn('_status', function($model){

						return "<span class='badge bg-success p-2 middle-text-badge'>{$model->status}</span>";

                    })
					->addColumn('_actions', function($model){

						return view('dataTables.movies.actions-column', compact('model'));

					})
					->rawColumns([
						'_name',
						'_publication_date',
						'_status',
						'_actions'
					]);
	}

}