<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{

    public function __construct()
    {
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::statement('TRUNCATE TABLE role_user');

        $users = [
            [
                'first_name' => 'Admin',
                'last_name' => 'admin',
                'email' => 'admin@movies.com',
                'password' => 'admin',
                'profile_pic' => 'assets/img/avatars/default.png',
                'is_active'=>1
            ],
            [
                'first_name' => 'Graham',
                'last_name' => 'Pareja',
                'email' => 'gpareja@movies.com',
                'password' => 'gpareja',
                'profile_pic' => 'assets/img/avatars/default.png',
                'is_active'=>1
            ],
            [
                'first_name' => 'Carlos',
                'last_name' => 'Torres',
                'email' => 'ctorres@movies.com',
                'password' => 'ctorres',
                'profile_pic' => 'assets/img/avatars/default.png',
                'is_active'=>1
            ],
            [
                'first_name' => 'Daniel',
                'last_name' => 'Aportela',
                'email' => 'daportela@movies.com',
                'password' => 'daportela',
                'profile_pic' => 'assets/img/avatars/default.png',
                'is_active'=>1
            ],
            [
                'first_name' => 'Gerardo',
                'last_name' => 'Rodriguez',
                'email' => 'grodriguez@movies.com',
                'password' => 'grodriguez',
                'profile_pic' => 'assets/img/avatars/default.png',
                'is_active'=>1
            ],
            [
                'first_name' => 'Fernando',
                'last_name' => 'Tejera',
                'email' => 'ftejera@movies.com',
                'password' => 'ftejera',
                'profile_pic' => 'assets/img/avatars/default.png',
                'is_active'=>1
            ],
            [
                'first_name' => 'Alonso',
                'last_name' => 'Martinez',
                'email' => 'amartinez@movies.com',
                'password' => 'amartinez',
                'profile_pic' => 'assets/img/avatars/default.png',
                'is_active'=>1
            ],
            [
                'first_name' => 'Juan',
                'last_name' => 'Castillo',
                'email' => 'admin@admin.com',
                'password' => 'admin',
                'profile_pic' => 'assets/img/avatars/default.png',
                'is_active'=>1
            ]
        ];

        foreach ($users as $user)
        {
            User::create($user);
        }
        
      

       
    }



}
