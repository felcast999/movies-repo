<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        #DB::connection('mysql2')->statement("SET FOREIGN_KEY_CHECKS = 0");

   
        $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
   

        DB::statement("SET FOREIGN_KEY_CHECKS = 1");
        #DB::connection('mysql2')->statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
