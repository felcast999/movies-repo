<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\User;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        
        $roles = [
            'super admin'=>'owner of system',
        	'admin' => 'Administrator',
        	'user' => 'System user',
        	'worker' => 'worker',
        ];


        foreach ($roles as $name => $description)
        {
        	$role = Role::create([
		        		'name' => $name,
		        		'display_name' => $name,
		        		'description' => $description
		        	]);

        }
        
        $admin_role = Role::where('name','admin')->first();
        $super_admin_role = Role::where('name','super admin')->first();
        $user_role = Role::where('name','user')->first();
        $worker_role = Role::where('name','worker')->first();

        // admin
        $admin = User::whereEmail('admin@movies.com')->first();
        $admin->roles()->attach([$admin_role->id, $super_admin_role->id]);

        // other users who need the boss role
        /*
        User::where('email','gpareja@inverellgroup.com')
        ->orWhere('email','ctorres@inverellgroup.com')
        ->orWhere('email','daportela@inverellgroup.com') 
        ->get()
        ->map(function($user) use($boss_role){ $user->roles()->attach($boss_role);});

        // users who need the worker role
        User::where('email','grodriguez@inverellgroup.com')
            ->orWhere('email','amartinez@inverellgroup.com')
            ->get()
            ->each(function($user) use($worker_role)
            {
                $user->roles()->attach($worker_role);
            });
*/
    }
}
