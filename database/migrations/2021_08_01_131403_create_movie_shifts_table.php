<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_shift', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('movie_id');
            $table->unsignedBigInteger('shift_id');

            $table->foreign('shift_id')->references('id')->on('shifts')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('movie_id')->references('id')->on('movies')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_shift');
    }
}
