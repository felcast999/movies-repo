<div class="file-content md">
<p>Proyecto de prueba para Humantech</p>
<ul >
<li>Clonar Repositorio</li>
<li >Ir a la raiz del proyecto y ejecutar el comando composer install</li>
<li>Ejecutar comando php artisan jwt:secret</li>
<li>ir a la carpeta resources/core_ui y ejecutar el comando npm install</li>
<li >Crear la base de datos y añadir archivo .env</li>
<li>Ejecutar migraciones y seed (php artisan migrate) (php artisan db:seed)</li>
<li >En carpeta resources/core_ui y ejecutar el comando ng build --prod</li>
<li >Ejecutar el comando php artisan storage:link</li>
<li >Ejecutar el comando php artisan serve</li>

</ul>
</div>